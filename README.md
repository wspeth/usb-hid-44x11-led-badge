# LED name badge controlled via USB by Raspberry Pi
Python code to transfer messages to the device

# The Device

It is less than 10 Euro, 92.3 * 28 mm in size and has provides a display that is a 44x11 matrix. The Interface is USB. A 160mAh Lithium battery is built in which is charged over USB within 90 minutes. When detached it can be turned off by a switch.
[![1pc-Special-Offer-44x11-Dots-Colorful-Rechargeable-Led-Scrolling-Name-Badge-Hot-Moving-Text-Display-Business_1_](https://gitlab.com/uploads/-/system/personal_snippet/1797388/a6d6f020fb8554d55c31ec532ad042d9/1pc-Special-Offer-44x11-Dots-Colorful-Rechargeable-Led-Scrolling-Name-Badge-Hot-Moving-Text-Display-Business_4_.jpg)](https://www.aliexpress.com/item/1pc-Special-Offer-44x11-Dots-Colorful-Rechargeable-Led-Scrolling-Name-Badge-Hot-Moving-Text-Display-Business/32859859251.html)


Upon connection to a Raspberry dmesg reports
> New USB device found, idVendor=0416, idProduct=5020
> Product: LS32 Custm HID
> Manufacturer: LSicroelectronics

The device comes with a mini CD containing a program (two actually) to transfer texts or images to the badge -- as many as 8 at a time and having length beyond 44 pixels which would be scrolled then. No support for Linux is provided. Therefore I started out to create a control program in python.

More than one badges can be connected to the raspberry pi and my code can handle this situation.

# Credits
Google rendered an article "Driving a scrolling LED badge from a Raspberry Pi"  posted 2013 by Dave Akerman in 2013[^4]. Device and Windows-Software look like mine but the protocol differs. Sadly enough his code got lost when he moved. Anyway this article gives some thorough insights.

On github there is a command line tool written in C by Happy Coding Robot[^5] who also did quite a bit of protocol reingeneering. While I failed to get his code compiled, I learned about the effect and speed attributes. Yet there were some pitfalls so in the end I did USB recording myself.

Free USB Analyzer[^6] served me well to understand the protocol and signal11#s ```hidapi``` -- A Simple library for communicating with USB and Bluetooth[^7] was a good starting point to establish communication with the device.

I also had a look at Dirk Reiner's "Little tool to program LED name badges"[^8], but his solution targets serial communication and so the device is on ```/dev/ttyUSB0``` -- unlike mine. Same holds for Kerry Schwab's "Python library to communicate with various models of programmable LED signs"[^9]


# Use cases
## clock
The badge can serve well as a clock display. Figures like HH:MM fit well in to the LED matrix using a raster font with 8x11 pixel per character. Such a font is on that CD by name of ASC11. I put this onto the raspberry. 

The time display being one message can be complemented by date info, temperature and the like which the device would play one after the other.

## world clock

Why not attach half a dozen such badges to the raspberry pi and create a "world clock". There is one caveat: the devices do not show up with a serial number on USB which might help telling which one is which. But for the world clock the display can switch between time and town to make clear which location the time refers to. When doing the mechanical setup I found it a nice idea to mount them left and right to a board so USB cable goes either left or right to the edge of the board. In other words: the right device(s) are mounted head-over. This requires the characters in the font to be turned upside down (which now my program can handle) -- and exchanging right scrolling settings with left scolling and vice versa.

## network addresses display
what I really was going for is the raspberry should display its network addresses (MAC and if assigned IP also) when booting -- which in the end I could manage in a really fabcy configuration: *udev rules* will start my program when the badge is plugged into the raspberry pi. 

# Preparation
## udev rules
The badges would show up as ```/dev/hidraw0``` and increasing digits for others. In fact there are higher numbered placeholders in ```/dev``` (even if no device is attached). Actually a ```hidraw``` can be quite a different device than one of those badges. I learned it is necessary to set access rights to be able to transfer data to the device and udev is the way to do it. In addition udev can take care for a renaming of the [^]device. 
ok, here are my ```udev rules```:
```
#LED badge
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0416", ATTRS{idProduct}=="5020", MODE="0660", GROUP="plugdev"
SUBSYSTEM=="hidraw", KERNEL=="hidraw*", SYMLINK+="led_badge%n" ATTRS{idVendor}=="0416", ATTRS{idProduct}=="5020", GROUP="plugdev", MODE="0666" ACTION=="add", RUN+="/usr/local/bin/led_badge.py %k"
```
Two things I want to point out: 
- SYMLINK: the devices show up now as ```/dev/led_badge<n>``` also when (and only when) plugged in.
- RUN: the python script ```/usr/local/bin/led_badge.py``` is started each time one of those badges is plugged in -- and it is given the ```led_badge<n>``` name as a parameter. Obviously the script is meant to write the display info into the badge.

To activate the udev rules first create the file with the above content then fire the command lines as follows:

```
sudo nano  /etc/udev/rules.d/99-led_badge.rules
sudo udevadm trigger
sudo udevadm control --reload-rules
```
This actually I found on stackexchange[^1] [^2] and at Daniel Drake's homepage[^3]
 
## installing libraries
I tried for many days to get the libs needed in place. I will have to set up a fresh raspbian and startover to check:
```
sudo apt-get install libhidapi-dev
sudo apt-get install libhidapi-libusb0
sudo apt-get install libhidapi-libusb0-dbg
pip install hid
# to display network addresses:
pip3 install netifaces
```
Just before frustration overwhelmed me I noticed that 
```
/home/pi/.local/lib/python3.5/site-packages/hid/__init__.py
```
required editing as the print statements were not in Python3 syntax.

# Run the code
The program is essentially a class Badge containing two inner classes Message and Font.
The font is loaded from file. While fonts like 5x7 raster or 12x16 can make sense -- and originally I started coding with such situations in mind -- my shortcuts finally went with the ASC11 font containing 8x11 raster characters.

For instatiation see the following examples
## examples:

```
    badge = Badge()
    badge = Badge(name = "led_badge0", brightness = Badge.BRIGHTNESS_MEDIUM)
    time_string = '{0:%H:%M}'.format(datetime.datetime.now())
    badge.add_message(time_string, speed=0, effect=Badge.EFFECT_FREEZE, flip=True)
    info = badge.send()
```

## attributes
Brightness is set per Badge and is valid for all messages added:
* BRIGHTNESS_MAX 
* BRIGHTNESS_HIGH
* BRIGHTNESS_MEDIUM 
* BRIGHTNESS_LOW

the effect is set per message and can be one of:
* EFFECT_LEFT
* EFFECT_RIGHT
* EFFECT_UP
* EFFECT_DOWN	
* EFFECT_FREEZE	
* EFFECT_ANIMATION
* EFFECT_SNOW	
* EFFECT_VOLUME	
* EFFECT_LASER	

other per message attributes have a range of valid values. Those are:
* speed (0..8)
* border (True, False)
* flash (True, False)
* flip (true, false)
* font

The flip and font settings are not transferred to the device actually. They are  internally used.


## communication
The USB communication is hardly anything else than:
```
import hid
device = hid.Device(path=("/dev/hidraw0").encode())
device.write(data) # one record
device.close()
```
yet the data structure is the secret sauce

## data structure
Records of 64 bytes each are sent to the device.
The first record is the header and tells about the length of max 8 messages and the effects to apply.
All other records are optional. They are required if one or more messages have lengths greater than zero.
The boundaries between messages lie within the records.
The messages transferred are not strings. They are bitmaps of 11 pixels height and their width counted in bytes not bits. For a single character message (assumed the character is a raster of 8x11 pixels) the bytes starting from 0 are the lines from top to bottom.

There is a method "dump_header" in the code that outputs the details of the built header.



# Things still to do
The code does not do any parameter value range checks currently. As marked in the code with ### notations it is a bit of work yet.

Situations like font file not accessible are not handled currently. 
The intention is to raise exceptions in such kind of situation.

I'd like to extend the code to handle a 5x7 raster font.

And last it appears some pixels appear wrong and I have to investigate on this. Current assumption is it is an issue with end-of-record and start-new-record in transmission.

I tried hard to get hidapi compiled on Windows for more comfort in developing, yet did not succeed.

As mentioned above I need a fresh install to ensure all the required and only the required modules are covered by this instructions. While trying I installed so many of them without a clear picture what's needed and what are dead ends for my objective. 

About the limitation in message length I am unsure and need some experimentation.
The code allows for a maximum of 255x11 bytes per message. Using the ASC11 font this means 255 text characters input. The device allows for up to 8 messages but I doubt they can each have the maximum length. Anyway if it would not be for different effects and speeds, why not stay with a single message?

# Author
Walter Speth, Germany, Cologne area
# References

[1]: [How to reload udev rules without reboot?](https://unix.stackexchange.com/questions/39370/how-to-reload-udev-rules-without-reboot)

[2]: [Getting /dev/hidraw# via udev for script](https://superuser.com/questions/1088992/getting-dev-hidraw-via-udev-for-script)

[3]: [Writing udev rules](http://reactivated.net/writing_udev_rules.html)

[4]: [Driving a scrolling LED badge from a Raspberry Pi](http://www.daveakerman.com/?p=1440)

[5]: [USB LED Badge commandline tool](https://github.com/HappyCodingRobot/USB_LED_Badge)

[6]: [Free USB Analyzer](https://freeusbanalyzer.com/features)

[7]: [HID API for Linux, Mac OS X, and Windows](http://www.signal11.us/oss/hidapi/)

[8]: [LED Name Badge Programmer](https://github.com/DirkReiners/LEDBadgeProgrammer)

[9]: [Python library to communicate with various models of programmable LED signs ](https://github.com/BrightLedSigns/pyledsign)