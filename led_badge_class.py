#!/usr/bin/python3
import struct
import hid
import ctypes
import sys
import time
import datetime

class Badge:

    DEVICE_PIXELS_WIDTH = 44
    DEVICE_PIXELS_HEIGHT = 11

    MESSAGE_MAXCOUNT = 0

    BRIGHTNESS_MAX = 0
    BRIGHTNESS_HIGH = 16
    BRIGHTNESS_MEDIUM = 32
    BRIGHTNESS_LOW = 48

    EFFECT_LEFT	=0
    EFFECT_RIGHT	=1
    EFFECT_UP	=2
    EFFECT_DOWN	=3
    EFFECT_FREEZE	=4
    EFFECT_ANIMATION	=5
    EFFECT_SNOW	=6
    EFFECT_VOLUME	=7
    EFFECT_LASER	=8

    HID_REPORT_TYPE = 0
    RECORD_LENGTH = 64

    class Font:
        font_lib=[]

        def __init__(self, file_name, char_height=11, char_width=8, flip=False):
            self.file_name = file_name
            self.char_height = char_height
            self.char_width = char_width
            self.flip = flip
            ### check fontlib if already loaded
            ### check if file exists
            with open(file_name, "rb") as f:
                self.font = f.read()
            if flip:
                copy = bytearray(self.font)
                for i in range(len(copy)):
                    i11 = i % 11
                    if i11 < 6:
                        j = i + 10 - i11 - i11
                        #print("x {0:4d} <--> {1:4d} {2:10d}".format(i,j,i11))
                        bi = struct.unpack_from('B', copy, i)[0]
                        bj = struct.unpack_from('B', copy, j)[0]
                        ##print(type(b))
                        ##print(b)
                        ##b = self.reverse(b)
                        ##print(type(b))
                        ##print(b)
                        struct.pack_into('B', copy, i, self.reverse(bj))
                        struct.pack_into('B', copy, j, self.reverse(bi))
                self.font = copy
            ##print (len(self.font))
            ##print (sys.getsizeof(self.font))

            Badge.Font.font_lib.append(self)

        def reverse(self,b):
            b_reversed = 0
            for i in range(8):
                b_reversed = (b_reversed << 1) | (b & 1)
                b = b >> 1
            return b_reversed

    class Message:
        def __init__(self, text='', effect=0, speed=1, flash=False, border=False, font=None, flip=False):
            self.text = text[::-1] if flip else text
            ##self.text = text
            self.char_count = len(text)
            self.speed = speed
            self.effect = effect
            self.flash = flash
            self.border = border
            self.flip = flip

            # assume 256 characters in file
            ### height and width might be determined from file size or file name
            self.font = Badge.Font(font, flip=self.flip)


    def __init__(self, name="hidraw0", brightness=0):
        self.name = name
        self.messages = []
        self.brightness = brightness

    def add_message(self, text='', effect=0, speed=1, flash=False, border=False, replace=None, font="ASC11", flip=False):
        ### implement replace
        ### implement font
        ### implement check on speed, effect
        ### implement check on len of text max 256
        index = len(self.messages)
        if flip:
            if effect == Badge.EFFECT_UP:
                effect = Badge.EFFECT_DOWN
            elif effect == Badge.EFFECT_DOWN:
                effect = Badge.EFFECT_UP
            elif effect == Badge.EFFECT_LEFT:
                effect = Badge.EFFECT_RIGHT
            elif effect == Badge.EFFECT_RIGHT:
                effect = Badge.EFFECT_LEFT

        self.messages.append(self.Message(text, effect, speed, flash, border, font, flip))
        return index

    def send(self):
        try:
            device = hid.Device(path=("/dev/" + self.name).encode())

        except Exception as x:
            device = None
            info = None
        ### check whether actually existent
        if device != None:
            info = (device.manufacturer, device.product, device.serial)

        reports = self.build_reports()
        i11=0
        i = 0
        for data in reports:
            if i > 0:
                pass
                #i11 =self.dump_record(data,i11=i11)
            if device != None:
                device.write(data)
#            with open('data.' +str(i), 'wb') as f:
#                f.write(data)
            i += 1
            #time.sleep(0.1)
        if device != None:
            device.close()
        else:
            sys.stdout.flush()
            #raise ValueError("led badge device could not be opened")
        return info

    def mask_build(self, bit_list):
        mask = 0
        for i in range(len(bit_list)):
            mask += bit_list[i] << i
        return mask.to_bytes(1, sys.byteorder)

    def speed_build(self, speed_effect_list):
        ### check len of list max 8
        ### check speed val 1..8
        ### check effect 0..8
        l = len(speed_effect_list)
        se_list = ['\0']*l
        for i in range(l):
            se_list[i] = ((speed_effect_list[i][0] << 4) + speed_effect_list[i][1]).to_bytes(1, sys.byteorder)
        return se_list

    def length_build(self, length_list):
        pass
        ### check max length per message
        ### check max number of messages
        return length_list

    def buffer_build(self):
        # each message may have different font hence different pixel width per char

        ### q, r = divmod(message.char_count * message.font.char_width, 8)  # how many bytes one scan line
        ### buff_len += message.font.char_height * (q if r == 0 else q + 1)  # () -> bytes per line)
        char_count = sum([len(message.text) for message in self.messages])

        ### short cut for first test:

        q,r = divmod(char_count * 11, Badge.RECORD_LENGTH)
        buffer_size = 11 * (q if r == 0 else q + 1)

        return ctypes.create_string_buffer(buffer_size)

    def dump_header(self,buffer):
        offset = 0
        fmt = '4scccc'
        prefix, terminator, brightness, mask_flash, mask_border =  struct.unpack_from(fmt, buffer, offset=offset)
        print("prefix:\t{}".format(prefix))
        print("terminator:\t{}".format(terminator))
        print("brightness:\t{}".format(brightness))
        print("flash:\t{:08b}".format(int.from_bytes(mask_flash,sys.byteorder)))
        print("border:\t{:08b}".format(int.from_bytes(mask_border,sys.byteorder)))
        offset += struct.calcsize(fmt)
        fmt='cccccccc'
        speed_effect = struct.unpack_from(fmt, buffer, offset=offset)
        print("speed_effect: ", end="")
        print(speed_effect)
        offset += struct.calcsize(fmt)
        fmt = 'HHHHHHHH'
        message_length = struct.unpack_from(fmt, buffer, offset=offset)
        ##print("message_length: ", end="")
        ##print(message_length)

    def dump_record(self,buffer,length=64,i11=0):

        fmt="B"
        for offset in range(length):
            b = struct.unpack_from(fmt, buffer, offset=offset)[0]
            pattern  = "{:08b}".format(b).translate(str.maketrans({'0': ' ', '1': '*'}))
            print("{0:2d} {1:2d}           {2:8s}".format(offset, i11 % 11, pattern))
            i11 += 1
        print()
        return i11


    def build_reports(self):
        brightness = self.brightness.to_bytes(1, sys.byteorder)
        mask_flash = self.mask_build([1 if message.flash else 0 for  message in self.messages])
        mask_border = self.mask_build([1 if message.border else 0 for  message in self.messages])
        speed_effect = self.speed_build([(message.speed,message.effect) for  message in self.messages])
        message_length = self.length_build([len(message.text).to_bytes(2, 'big') for  message in self.messages])

        buff_len = 0
        for message in self.messages:
            q,r = divmod(message.char_count * message.font.char_width,8) # how many bytes one scan line
            buff_len += message.font.char_height * (q if r == 0 else q + 1) # () -> bytes per line)

        q,r = divmod(buff_len,Badge.RECORD_LENGTH)
        report = [ctypes.create_string_buffer(Badge.RECORD_LENGTH) for i in range (1 + (q if r == 0 else q+1))]

        offset = 0

        fmt = '5sccc'
        struct.pack_into(fmt, report[0], offset, b'wang\0', brightness, mask_flash, mask_border)
        offset += struct.calcsize(fmt)
        ##print('offset now: ' + str(offset))

        fmt = 'c'
        for i in range(8):
            if i < len(self.messages):
                ##print("speed_effect[{0}]: ".format(i),end="")
                ##print(speed_effect[i])
                struct.pack_into(fmt, report[0], offset, speed_effect[i])
            else:
                ##print("speed_effect[{0}]: is 0".format(i))
                struct.pack_into(fmt, report[0], offset, b'\0')
            offset += struct.calcsize(fmt)


        fmt = 'bb'
        for i in range(8):
            if i < len(self.messages):
                ##print("length of message {}".format(message_length[i]))
                struct.pack_into(fmt, report[0], offset, message_length[i][0], message_length[i][1])#int.from_bytes(message_length[i], byteorder='big'))
            else:
                struct.pack_into(fmt, report[0], offset, 0,0)
            offset += struct.calcsize(fmt)

        if 1==0: # for debugging only
            self.dump_header(report[0])

        index_record = 1
        offset = 0
        k = 0
        for message in self.messages:
            for l in range(len(message.text)):
                #print()
                for row in range(message.font.char_height):
                    scan_line = struct.unpack_from('B',message.font.font,message.font.char_height * ord(message.text[l]) + row)[0]
                    ##print("{:08b}".format(scan_line))
                    ##pattern  = "{:08b}".format(scan_line).translate(str.maketrans({'0': ' ', '1': '*'}))
                    ##print("{0:1d} {1:3d} {2:2d} {3:2d}           {4:8s}".format(k,l,offset,row, pattern))
                    struct.pack_into("B", report[index_record], offset, scan_line)
                    offset = (offset + 1) % Badge.RECORD_LENGTH
                    if offset == 0:
                        index_record += 1
            k = k + 1

        return report



if __name__ == "__main__":
    #badges = [Badge(name = arg) for arg in sys.argv[1:]]
    device_name = "led_badge0"
    badge = Badge(name = device_name, brightness = Badge.BRIGHTNESS_MEDIUM)

    time_string = '{0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
    badge.add_message(time_string, speed=0, effect=Badge.EFFECT_FREEZE, flip=True)
    info = badge.send()


    
    ##print (info) #('LSicroelectronics', 'LS32 Custm HID', '')
