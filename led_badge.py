#!/usr/bin/python3
#/usr/local/bin/led_badge.py
import os
import syslog

old_path = os.getcwd()
os.chdir(os.path.dirname(os.path.realpath(__file__)))
syslog.syslog("led_badge changed working dir from {}  to {}".format(old_path, os.getcwd()))

import sys
import hid
import netifaces as nif

from led_badge_class import *



def endpoints():
    # Returns a list of MACs for interfaces & given IP
    interfaces = {}

    for i in nif.interfaces():
        if i != "lo":
            addrs = nif.ifaddresses(i)
            ##print(addrs)
            if_mac = ""
            if_ip = ""
            try:
                if_mac = addrs[nif.AF_LINK][0]['addr']
                if_ip = addrs[nif.AF_INET][0]['addr']
            except:
                pass
            interfaces[i] = (if_mac, if_ip)
    return interfaces


if __name__ == "__main__":

    path = "hidraw0"
    if len(sys.argv) > 1:
        path = sys.argv[1]
    syslog.syslog("led_badge arg: " + path)
    badge=Badge(path)

    lines=[""]*8
    for line in lines:
        badge.add_message(line, speed=2, font="ASC11")
    badge.send()

    badge=Badge(path)
    interfaces=endpoints()
    lines = ["{}: {} {}".format(interface,interfaces[interface][0],interfaces[interface][1]) for interface in interfaces]
    for line in lines:
        #print(line)
        badge.add_message(line, speed=2, font="ASC11")
        syslog.syslog(line)
    badge.send()
    # sudo tail /var/log/syslog

